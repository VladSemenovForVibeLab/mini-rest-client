package ru.semenov.util;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.util.concurrent.atomic.AtomicInteger;

@ApplicationScoped
@RequiredArgsConstructor
public class EmailCreatedUtil {
    @ConfigProperty(name = "candidate-api.email", defaultValue = "ooovladislavchik@gmail.com")
    String email;

    public String generateEmail() {
        return String.format("%s",
                email);
    }
}

package ru.semenov.util;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.enums.Roles;

import java.util.List;

import static ru.semenov.constant.RoleConstants.*;

@ApplicationScoped
public class RoleUtil {
    public static Uni<String> selectRole(List<String> roles, Roles roleRequest) {
        return Uni.createFrom()
                .item(roles)
                .onItem()
                .transform(roleList -> roleList.stream()
                        .filter(role -> matchesRole(role, roleRequest))
                        .findFirst()
                        .orElse(null));
    }

    private static boolean matchesRole(String role, Roles roleRequest) {
        return switch (roleRequest) {
            case Tester -> role.equalsIgnoreCase(TESTER);
            case Java_Developer -> role.equalsIgnoreCase(JAVA_DEVELOPER);
            case Systems_Analyst -> role.equalsIgnoreCase(SYSTEMS_ANALYST);
            case JS_React_Developer -> role.equalsIgnoreCase(JS_REACT_DEVELOPER);
            case Application_Administrator -> role.equalsIgnoreCase(APPLICATION_ADMINISTRATOR);
            default -> false;
        };
    }
}

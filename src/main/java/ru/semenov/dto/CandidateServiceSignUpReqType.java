package ru.semenov.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
public class CandidateServiceSignUpReqType {
    String last_name;
    String first_name;
    String email;
    String role;
}

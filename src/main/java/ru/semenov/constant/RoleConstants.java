package ru.semenov.constant;

public interface RoleConstants {
    String TESTER = "Тестировщик";
    String JAVA_DEVELOPER ="Разработчик Java";
    String SYSTEMS_ANALYST = "Системный аналитик";
    String JS_REACT_DEVELOPER = "Разработчик JS/React";
    String APPLICATION_ADMINISTRATOR = "Прикладной администратор";
}

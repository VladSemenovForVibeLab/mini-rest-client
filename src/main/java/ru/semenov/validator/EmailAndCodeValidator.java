package ru.semenov.validator;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

import static java.util.Objects.isNull;

@ApplicationScoped
public class EmailAndCodeValidator {
    public Uni<Void> validateEmailAndCode(String email, String code) {
        return isNull(email) || isNull(code)
                ? Uni.createFrom().failure(IllegalArgumentException::new)
                : Uni.createFrom().voidItem();
    }
}

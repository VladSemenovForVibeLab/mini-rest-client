package ru.semenov.configuration;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Default;
import jakarta.enterprise.inject.Produces;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

@ApplicationScoped
public class RestTemplateConfig {

    @Produces
    @Default
    public RestTemplate createRestTemplate() {
        return new RestTemplate();
    }
    @Produces
    @Default
    public RestTemplateBuilder createRestTemplateBuilder() {
        return new RestTemplateBuilder();
    }
}

package ru.semenov.enums;

public enum Roles {
    Systems_Analyst,
    Java_Developer,
    JS_React_Developer,
    Tester,
    Application_Administrator
}

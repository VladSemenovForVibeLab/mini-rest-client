package ru.semenov.builder;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import ru.semenov.dto.CandidateServiceSignUpReqType;
import ru.semenov.util.EmailCreatedUtil;

@ApplicationScoped
@RequiredArgsConstructor
public class CreateCandidateRequestBuilder {
    private final EmailCreatedUtil emailCreatedUtil;
    @ConfigProperty(name = "candidate-api.firstName",defaultValue = "Владислав")
    String firstName;
    @ConfigProperty(name = "candidate-api.lastName", defaultValue = "Семенов")
    String lastName;

    public Uni<CandidateServiceSignUpReqType> build(String role) {
        return Uni.createFrom()
                .item(new CandidateServiceSignUpReqType())
                .onItem()
                .transform(candidateServiceSignUpReqType -> {
                    candidateServiceSignUpReqType.setRole(role);
                    candidateServiceSignUpReqType.setEmail(emailCreatedUtil.generateEmail());
                    candidateServiceSignUpReqType.setFirst_name(firstName);
                    candidateServiceSignUpReqType.setLast_name(lastName);
                    return candidateServiceSignUpReqType;
                });
    }

}

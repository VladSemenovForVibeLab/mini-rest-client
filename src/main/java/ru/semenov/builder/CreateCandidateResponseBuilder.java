package ru.semenov.builder;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.dto.CandidateServiceSignUpReqType;
import ru.semenov.dto.CandidateServiceSignUpRespType;

@ApplicationScoped
public class CreateCandidateResponseBuilder {
    public Uni<CandidateServiceSignUpRespType> build(CandidateServiceSignUpReqType candidateServiceSignUpReqType){
        return Uni.createFrom()
                .item(CandidateServiceSignUpRespType::new)
                .onItem()
                .transform(candidate -> {
                    candidate.setString("Данные внесены");
                    candidate.setEmail(candidateServiceSignUpReqType.getEmail());
                    return candidate;
                });
    }
}

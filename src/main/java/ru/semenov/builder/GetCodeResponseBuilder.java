package ru.semenov.builder;

import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.dto.CandidateServiceGetCodeRespType;
import ru.semenov.dto.CandidateServiceSignUpRespType;

@ApplicationScoped
public class GetCodeResponseBuilder {
    public CandidateServiceGetCodeRespType build(String forEntity,
                                                      CandidateServiceSignUpRespType candidateServiceSignUpRespType) {
        return CandidateServiceGetCodeRespType.builder()
                        .code(forEntity)
                        .email(candidateServiceSignUpRespType.getEmail())
                        .message("Все окей")
                        .build();
    }
}

package ru.semenov.builder;

import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.dto.CandidateServiceSetStatusRespType;

import java.time.LocalDateTime;

@ApplicationScoped
public class SetStatusResponseBuilder {
    public CandidateServiceSetStatusRespType build() {
        return CandidateServiceSetStatusRespType
                .builder()
                .message("Статус increased зафиксирован. Задание выполнено")
                .now(LocalDateTime.now())
                .build();
    }
}

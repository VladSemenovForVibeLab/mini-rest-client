package ru.semenov.builder;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.dto.CandidateServiceSetStatusReqType;

import static ru.semenov.constant.StatusConstant.INCREASED;

@ApplicationScoped
public class SetStatusRequestBuilder {

    public Uni<CandidateServiceSetStatusReqType> build(String s) {
        return Uni.createFrom()
               .item(()-> CandidateServiceSetStatusReqType.builder()
                       .status(INCREASED)
                       .token(s)
                       .build());
    }
}

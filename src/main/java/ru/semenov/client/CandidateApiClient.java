package ru.semenov.client;

import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import ru.semenov.dto.CandidateServiceGetRolesResType;


@RegisterRestClient(baseUri = "http://193.19.100.32:7000")
public interface CandidateApiClient {
    @GET
    @Path("/api/get-roles")
    Uni<CandidateServiceGetRolesResType> getRoles();

}

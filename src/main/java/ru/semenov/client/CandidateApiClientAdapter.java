package ru.semenov.client;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.SneakyThrows;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import ru.semenov.builder.CreateCandidateRequestBuilder;
import ru.semenov.builder.CreateCandidateResponseBuilder;
import ru.semenov.builder.GetCodeResponseBuilder;
import ru.semenov.builder.SetStatusResponseBuilder;
import ru.semenov.dto.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ApplicationScoped
public class CandidateApiClientAdapter {
    private final RestTemplate restTemplate;
    @RestClient
    CandidateApiClient candidateApiClient;
    private final CreateCandidateRequestBuilder createCandidateRequestBuilder;
    private final CreateCandidateResponseBuilder createCandidateResponseBuilder;
    private final GetCodeResponseBuilder getCodeResponseBuilder;
    private final SetStatusResponseBuilder statusResponseBuilder;

    public CandidateApiClientAdapter(RestTemplateBuilder restTemplateBuilder, CreateCandidateRequestBuilder createCandidateRequestBuilder, CreateCandidateResponseBuilder createCandidateResponseBuilder, GetCodeResponseBuilder getCodeResponseBuilder, SetStatusResponseBuilder statusResponseBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        this.getCodeResponseBuilder = getCodeResponseBuilder;
        this.statusResponseBuilder = statusResponseBuilder;
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        messageConverters.add(converter);
        this.restTemplate.setMessageConverters(messageConverters);
        this.createCandidateRequestBuilder = createCandidateRequestBuilder;
        this.createCandidateResponseBuilder = createCandidateResponseBuilder;
    }

    public Uni<CandidateServiceGetRolesResType> getRoles() {
        return candidateApiClient.getRoles();
    }

    public Uni<CandidateServiceSignUpRespType> signUp(String role) {
        return Uni.createFrom()
                .nullItem()
                .flatMap(ignored -> createCandidateRequestBuilder.build(role))
                .flatMap(request -> signUpRequest(request)
                        .onItem()
                        .ifNull()
                        .failWith(new RuntimeException()))
                .flatMap(createCandidateResponseBuilder::build);
    }

    private Uni<CandidateServiceSignUpReqType> signUpRequest(CandidateServiceSignUpReqType request) {
        return Uni.createFrom()
                .nullItem()
                .onItem()
                .transform(ignored -> {
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    HttpEntity<CandidateServiceSignUpReqType> reqTypeHttpEntity = new HttpEntity<>(request, headers);
                    restTemplate.postForEntity("http://193.19.100.32:7000/api/sign-up",
                            reqTypeHttpEntity,
                            String.class);
                    return request;
                });

    }

    @SneakyThrows
    public Uni<CandidateServiceGetCodeRespType> getCode(CandidateServiceSignUpRespType candidateServiceSignUpRespType) {
        return Uni.createFrom()
                .item(candidateServiceSignUpRespType.getEmail())
                .onItem()
                .transform(email -> getCodeResponseBuilder.build(
                        restTemplate.getForObject("http://193.19.100.32:7000/api/get-code?email={email}",
                                String.class,
                                email),
                        candidateServiceSignUpRespType));
    }

    public Uni<CandidateServiceSetStatusRespType> setStatus(CandidateServiceSetStatusReqType candidateServiceSetStatusReqType) {
        return Uni.createFrom()
                .nullItem()
                .onItem()
                .transform(ignored -> {
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    HttpEntity<CandidateServiceSetStatusReqType> reqTypeHttpEntity = new HttpEntity<>(candidateServiceSetStatusReqType, headers);
                    restTemplate.postForObject("http://193.19.100.32:7000/api/set-status",
                            reqTypeHttpEntity,
                            String.class);
                    return statusResponseBuilder.build();
                });
    }
}

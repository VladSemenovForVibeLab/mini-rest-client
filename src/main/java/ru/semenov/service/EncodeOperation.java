package ru.semenov.service;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import ru.semenov.builder.SetStatusRequestBuilder;
import ru.semenov.dto.CandidateServiceGetCodeRespType;
import ru.semenov.dto.CandidateServiceSetStatusReqType;
import ru.semenov.validator.EmailAndCodeValidator;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@ApplicationScoped
@RequiredArgsConstructor
public class EncodeOperation {
    private final EmailAndCodeValidator emailAndCodeValidator;
    private final SetStatusRequestBuilder setStatusRequestBuilder;
    public Uni<CandidateServiceSetStatusReqType> encode(CandidateServiceGetCodeRespType candidateServiceGetCodeRespType) {
        var emailFromRequest = candidateServiceGetCodeRespType.getEmail();
        var codeFromRequest = candidateServiceGetCodeRespType.getCode();
        return Uni.createFrom()
                .nullItem()
                .flatMap(ignored -> emailAndCodeValidator.validateEmailAndCode(emailFromRequest,
                        codeFromRequest)
                .flatMap(valid -> encodeEmailWithCode(emailFromRequest,
                        codeFromRequest)))
                .flatMap(setStatusRequestBuilder::build);
    }
    @SneakyThrows
    private Uni<String> encodeEmailWithCode(String email, String code) {
        return Uni.createFrom()
                .item(email + ":" + code)
                .onItem()
                .transform(item -> Base64
                        .getEncoder()
                        .encodeToString(item.getBytes(StandardCharsets.UTF_8)));
    }
}

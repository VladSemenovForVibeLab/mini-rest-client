package ru.semenov.service;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import ru.semenov.client.CandidateApiClientAdapter;
import ru.semenov.dto.CandidateServiceSetStatusRespType;
import ru.semenov.enums.Roles;

import static ru.semenov.util.RoleUtil.selectRole;

@ApplicationScoped
@RequiredArgsConstructor
public class CallOperation {
    private final CandidateApiClientAdapter candidateApiClientAdapter;
    private final EncodeOperation encodeOperation;

    public Uni<CandidateServiceSetStatusRespType> callMethods() {
        return Uni.createFrom()
                .nullItem()
                .chain(candidateApiClientAdapter::getRoles)
                .chain(roles -> selectRole(roles.getRoles(), Roles.Java_Developer))
                .chain(candidateApiClientAdapter::signUp)
                .chain(candidateApiClientAdapter::getCode)
                .chain(encodeOperation::encode)
                .chain(candidateApiClientAdapter::setStatus);
    }
}

package ru.semenov.controller;

import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import ru.semenov.dto.CandidateServiceSetStatusRespType;
import ru.semenov.service.CallOperation;

@Path("/api/v1/call")
@RequiredArgsConstructor
public class CallController {
    private final CallOperation callOperation;
    @GET
    public Uni<CandidateServiceSetStatusRespType> callOperation(){
        return callOperation.callMethods();
    }
}
